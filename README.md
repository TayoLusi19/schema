# schema



## name_basics table

```bash
CREATE TABLE name_basics (
    nconst VARCHAR(10) PRIMARY KEY,
    primaryName VARCHAR(255),
    birthYear VARCHAR(4), 
    deathYear VARCHAR(4), 
    primaryProfession VARCHAR(255),  
    knownForTitles VARCHAR(255)  
);
```

## title_ratings table

```bash
CREATE TABLE title_ratings (
    tconst VARCHAR(10) PRIMARY KEY,
    averageRating DECIMAL(3,1),
    numVotes INT
);
```

## title_basics table
```bash
CREATE TABLE title_basics (
    tconst VARCHAR(10) PRIMARY KEY,
    titleType VARCHAR(20),
    primaryTitle VARCHAR(255),
    originalTitle VARCHAR(255),
    isAdult INTEGER, 
    startYear VARCHAR(4), 
    endYear VARCHAR(4), 
    runtimeMinutes VARCHAR(5),
    genres VARCHAR(255) 
);
```


## title_crew table

```bash
CREATE TABLE title_crew (
    tconst VARCHAR(10) PRIMARY KEY,
    directors VARCHAR(255), 
    writers VARCHAR(255)    
);
```

## title_episode table

```bash
CREATE TABLE title_episode (
    tconst VARCHAR(10) PRIMARY KEY,
    parentTconst VARCHAR(10),
    seasonNumber VARCHAR(5), 
    episodeNumber VARCHAR(5)  
);
```

## title_principals table
```bash
CREATE TABLE title_principals (
    tconst VARCHAR(10),
    ordering INTEGER,
    nconst VARCHAR(10),
    category VARCHAR(50),
    job VARCHAR(255), 
    characters TEXT, 
    PRIMARY KEY (tconst, ordering) 
);
```

## title_akas

```bash
CREATE TABLE title_akas (
    titleId VARCHAR(10),
    ordering INTEGER,
    title VARCHAR(255),
    region VARCHAR(10),
    language VARCHAR(10),
    types VARCHAR(50),
    attributes VARCHAR(255),
    isOriginalTitle BOOLEAN,
    PRIMARY KEY (titleId, ordering)
);
```


















